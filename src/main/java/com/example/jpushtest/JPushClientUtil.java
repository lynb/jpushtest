package com.example.jpushtest;

import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class JPushClientUtil {

    private static Logger logger = LoggerFactory.getLogger(JPushClientUtil.class);
    private static final String appKey = "ed175096fc8e141a633d6bb5";
    private static final String masterSecret = "8945240b1a808549ae86ccab";
    protected static JPushClient jpush = null;
    private static final int MAX = Integer.MAX_VALUE;
    private static boolean flag = false;

    public static void main(String[] args) {
        String msgContent = "看到信息了么，看到就推送成功了！";
        Map<String, String> extras = new HashMap<String, String>();
        // 添加附加信息
        extras.put("url", "http://www.baidu.com");
        sendAllsetNotification(msgContent, extras);
    }

    private static void init() {
        if (!flag) {
            jpush = new JPushClient(masterSecret, appKey);
            flag = true;
        }
    }


    /**
     * +     * 推送所有信息
     * +     * @param msgContent
     * +
     */
    public static void pushMessage(String msgContent) {

        init();
        PushResult pushResult;
        try {
            pushResult = jpush.sendMessageAll(msgContent);
            if (pushResult.isResultOK()) {
                logger.info("发送成功");
            }
        } catch (APIConnectionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (APIRequestException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    /**
     * 给所有平台的所有用户发通知
     */
    public static void sendAllsetNotification(String message, Map<String, String> extras) {

        init();
        PushPayload payload = buildPushObject_all_alias_alert(message, extras);
        try {
            PushResult result = jpush.sendPush(payload);
            logger.info(result.toString());
        } catch (APIConnectionException e) {
            logger.info("推送失败!", e);
        } catch (APIRequestException e) {
            logger.info("APIRequestException" + e);
            logger.info("Error Message: " + e.getErrorMessage());
            System.out.println("Error Message: " + e.getErrorMessage());
        }
    }


    /**
     * 推送 通知
     */
    public static void pushNotification(String title, String alert, String... alias) {
        init();
        Map extras = new HashMap();
        PushResult pushResult1;
        PushResult pushResult2;
        try {
            pushResult1 = jpush.sendAndroidNotificationWithAlias(title, alert, extras, alias);
            pushResult2 = jpush.sendIosNotificationWithAlias(alert, extras, alias);
            //jpush.
            if (pushResult1.isResultOK()) {
                logger.info("android发送成功");
            } else {
                logger.info("android发送失败");
            }
            if (pushResult2.isResultOK()) {
                logger.info("Ios发送成功");
            }
        } catch (APIConnectionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (APIRequestException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 推送ANDROID客户端通知
     */
    public static void pushAndroidNotification(String title, Map<String, String> extras, String alert, String... alias) {
        init();
        PushResult pushResult1;
        try {
            pushResult1 = jpush.sendAndroidNotificationWithAlias(title, alert, extras, alias);
            if (pushResult1.isResultOK()) {
                logger.info("android发送成功");
            } else {
                logger.info("android发送失败");
            }

        } catch (APIConnectionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (APIRequestException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 推送Ios通知
     */
    public static void pushIosNotification(String alert, Map<String, String> extras, String... alias) {
        init();
        PushResult pushResult2;
        try {
            pushResult2 = jpush.sendIosNotificationWithAlias(alert, extras, alias);

            if (pushResult2.isResultOK()) {
                logger.info("Ios发送成功");
            } else {
                logger.info("Ios发送失败");
            }
        } catch (APIConnectionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (APIRequestException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 推送 通知
     */
    public static void pushNotification(String alert, String alias) {
        init();
        Map extras = new HashMap();
        extras.put("1", "1");
        PushResult pushResult2;
        try {
            pushResult2 = jpush.sendIosNotificationWithAlias(alert, extras, alias);
            if (pushResult2.isResultOK()) {
                logger.info("android发送成功");
            } else {
                logger.info("android发送失败");
            }
        } catch (APIConnectionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (APIRequestException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    /**
     * 发送通知
     *
     * @param message
     * @param extras
     * @return
     * @author WangMeng
     *
     */
    private static PushPayload buildPushObject_all_alias_alert(String message,
                                                               Map<String, String> extras) {
        return PushPayload.newBuilder()
                .setPlatform(Platform.android_ios())
                // 设置平台
                .setAudience(Audience.all())
                // 按什么发送 tag alia
                .setNotification(
                        Notification
                                .newBuilder()
                                .setAlert(message)
                                .addPlatformNotification(
                                        AndroidNotification.newBuilder().addExtras(extras).build())
                                .addPlatformNotification(
                                        IosNotification.newBuilder().addExtras(extras).build())
                                .build())
                // 发送消息
                .setOptions(Options.newBuilder().setApnsProduction(false).build()).build();
    }
}