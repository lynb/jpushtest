package com.example.jpushtest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @PostMapping("/a")
    public String test() {
        return "a";
    }

    @GetMapping("/b")
    public String tests() {
        return "b";
    }

}
