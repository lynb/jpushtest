function setCookie(name, value, iDay) {
	clearCookie();
	/* iDay 表示过期时间   
	cookie中 = 号表示添加，不是赋值 */
	var oDate = new Date();
	oDate.setDate(oDate.getDate() + iDay);
	document.cookie = name + '=' + value + ((iDay == null) ? '' : ';expires=' + oDate);
}

function getCookie() {
	var strCookie = document.cookie;
	var arr = strCookie.split("=");
	username = arr[0];
	pwd = arr[1]
}

function clearCookie() {
	var keys = document.cookie.match(/[^ =;]+(?=\=)/g);
	if (keys) {
		for (var i = keys.length; i--;)
			document.cookie = keys[i] + '=0;expires=' + new Date(0).toUTCString()
	}
}